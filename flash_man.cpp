/************************************************************************//**
 * \file
 *
 * \brief Flash Manager.
 *
 * Handles basic operations on flash chips (Program/Read/Erase) using MDMA
 * interface.
 *
 * \author doragasu
 * \date   2017
 ****************************************************************************/
#include <QApplication>
#include <stdlib.h>
#include "flash_man.h"
#include "util.h"
#include "commands.h"
#include "mdma.h"

/********************************************************************//**
 * Program a file to the flash chip.
 *
 * \param[in] filename  File name to program.
 * \param[in] autoErase Erase the flash range where the file will be
 *            flashed.
 * \param[in] start     Word memory addressh where the file will be
 *            programmed.
 * \param[in] len       Number of words to write to the flash.
 *
 * \return A pointer to a buffer containing the file to flash (byte
 * swapped) or NULL if the operation fails.
 *
 * \warning The user is responsible of freeing the buffer calling
 * BufFree() when its contents are not needed anymore.
 ************************************************************************/
uint8_t *FlashMan::Program(const char filename[], bool autoErase,
		uint32_t *start, uint32_t *len)
{
	FILE *rom;
	uint8_t *writeBuf;
	uint32_t addr;
	uint32_t toWrite;
	uint32_t i;

	// Open the file to flash
	if (!(rom = fopen(filename, "rb"))) return NULL;

	// Obtain length if not specified
	if (!(*len)) {
		fseek(rom, 0, SEEK_END);
		*len = ftell(rom);
		fseek(rom, 0, SEEK_SET);
	}

	writeBuf = (uint8_t*)malloc(*len);
	if (!writeBuf) {
		fclose(rom);
		return NULL;
	}
	fread(writeBuf, *len, 1, rom);
	fclose(rom);

	// If requested, perform auto-erase
	if (autoErase) {
		emit StatusChanged("Auto erasing");
		QApplication::processEvents();
		DelayMs(1);
		if (MDMA_range_erase(*start, *len)) {
			free(writeBuf);
			return NULL;
		}
	}

	emit RangeChanged(0, *len);
	emit ValueChanged(0);
	emit StatusChanged("Program...");
	QApplication::processEvents();

	for (i = 0, addr = *start; i < *len;) {
		toWrite = MIN(TRANSFER_LEN_MAX, *len - i);
		if (MDMA_write(toWrite, addr, writeBuf + i)) {
			free(writeBuf);
			fclose(rom);
			return NULL;
		}
		// Update vars and draw progress bar
		i += toWrite;
		addr += toWrite;
		emit ValueChanged(i);
		QApplication::processEvents();
	}
	emit ValueChanged(i);
	emit StatusChanged("Done!");
	QApplication::processEvents();
	return writeBuf;
}

/********************************************************************//**
 * Read a memory range from the flash chip.
 *
 * \param[in] start Word memory address to start reading from.
 * \param[in] len   Number of words to read from flash.
 *
 * \return A pointer to the buffer containing the data read from the
 * flash, or NULL if the read operation has failed.
 *
 * \warning The user is responsible of freeing the buffer calling
 * BufFree() when its contents are not needed anymore.
 ************************************************************************/
uint8_t *FlashMan::Read(uint32_t start, uint32_t len)
{
	uint8_t *readBuf;
	uint32_t toRead;
	uint32_t addr;
	uint32_t i;

	emit RangeChanged(0, len);
	emit ValueChanged(0);
	emit StatusChanged("Reading");
	QApplication::processEvents();

	readBuf = (uint8_t*)malloc(len);
	if (!readBuf) {
		return NULL;
	}

	for (i = 0, addr = start; i < len;) {
		toRead = MIN(TRANSFER_LEN_MAX, len - i);
		if (MDMA_read(toRead, addr, readBuf + i)) {
			free(readBuf);
			return NULL;
		}
		// Update vars and draw progress bar
		i += toRead;
		addr += toRead;
		emit ValueChanged(i);
		QApplication::processEvents();
	}
	emit ValueChanged(i);
	emit StatusChanged("Done");
	QApplication::processEvents();
	return readBuf;
}

/********************************************************************//**
 * Erases a memory range from the flash chip.
 *
 * \param[in] start Word memory address of the beginning of the range
 *            to erase.
 * \param[in] len   Length (in words) of the range to erase.
 *
 * \return 0 on success, non-zero if erase operation fails.
 ************************************************************************/
int FlashMan::RangeErase(uint32_t start, uint32_t len)
{
	if (MDMA_range_erase(start, len)) return -1;

	return 0;
}

/********************************************************************//**
 * Issues a complete chip erase command to the flash chip.
 *
 * \return 0 on success, non-zero if erase operation fails.
 ************************************************************************/
int FlashMan::FullErase(void)
{
	if (MDMA_cart_erase()) return -1;

	return 0;
}

/********************************************************************//**
 * Frees a buffer previously allocated by Program() or Read().
 *
 * \param[in] buf The address of the buffer to free.
 ************************************************************************/
void FlashMan::BufFree(uint8_t *buf)
{
	free(buf);
}

/********************************************************************//**
 * Obtains the flash chip 8-bit Manufacturer ID code.
 *
 * \param[out] manId The 8-bit Manufacturer ID of the flash chip.
 *
 * \return 0 on success, non-zero on error.
 ************************************************************************/
uint16_t FlashMan::ManIdGet(uint8_t *manId)
{
	return MDMA_manId_get(manId);
}

/********************************************************************//**
 * Obtains the flash chip 8-bit Device ID codes.
 *
 * \param[out] devIds The 8-bit Device IDs of the flash chip.
 * \param[inout] numIds On input, it holds the devIds buffer length. On
 *               output it has the number of ids the device has.
 *
 * \return 0 on success, non-zero on error.
 ************************************************************************/
uint16_t FlashMan::DevIdGet(uint8_t *devIds, uint8_t *numIds)
{
	return MDMA_devId_get(devIds, numIds);
}

/********************************************************************//**
 * Enters DFU bootloader mode.
 *
 * \return 0 on success, non-zero on error.
 ************************************************************************/
uint16_t FlashMan::DfuBootloader(void)
{
	return MDMA_bootloader();
}

uint16_t FlashMan::CartInit(void)
{
	InitData d = {};

	return MDMA_cart_init(&d);
}

uint16_t FlashMan::CartTypeSet(uint8_t key)
{
	return MDMA_cart_type_set((MdmaCartType)key);
}
